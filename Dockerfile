FROM nginx:alpine

ENV NOTION_PROXY_PROTO http
ENV NOTION_PROXY_HOST change.me.example.com
ENV NOTION_PROXY_TARGET_PATH My-Notion-Article-123454231r12r321re1f

COPY nginx.conf.tmpl /nginx.conf.tmpl
COPY run-nginx.sh /usr/local/bin/run-nginx.sh

CMD /usr/local/bin/run-nginx.sh
