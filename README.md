# notion-proxy for xbrp.commonground.nl

Manually deployed to the `xbrp-notion` namespace on the `cluster-dv-prod-common.haven.vng.cloud.` [Haven](https://haven.commonground.nl) cluster.

```bash
docker build -t registry.gitlab.com/commonground/xbrp/notion-proxy .

docker push registry.gitlab.com/commonground/xbrp/notion-proxy

kubectl \
	--context pinniped-azure-common-prod \
	apply -f notion-proxy.yaml
```
