#!/bin/ash

set -euxo pipefail

export DOLLAR='$'
envsubst < /nginx.conf.tmpl > /etc/nginx/conf.d/default.conf

nginx -g "daemon off;"
